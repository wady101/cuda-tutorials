//#include<cuda.h> if using nvcc
#include<cuda_runtime.h>
#include<stdio.h>

// copied from https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
// required for error checking 
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

#define ROWS 4
#define COLS 4
// __global__
// void vecAddKerneln(float* *A, float* *B, float* *C, int n, int m) {
// int i = threadIdx.x + blockDim.x * blockIdx.x;
// if(i<n) C[i][m] = A[i][m] + B[i][m];  
// }
__global__
void vecAddKernelm(float A[][COLS], float B[][COLS], float C[][COLS], int n, int m) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    if (i < n && j < m)
        C[i][j] = A[i][j] + B[i][j];
    }
template<int m>
void VecAdd(float A[][m], float B[][m], float C[][m], int n) {
    int size = m * n * sizeof(float);
    float (*d_A)[COLS], (*d_B)[COLS], (*d_C)[COLS];
    cudaError_t err = cudaMalloc((void **) &d_A, size);
    if (err != cudaSuccess) {
        printf("%s in %s at line %d\n", cudaGetErrorString( err),
        __FILE__, __LINE__);
        exit(EXIT_FAILURE);
        }
        
    cudaError_t err_a = cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
    if (err_a != cudaSuccess) {
        printf("%s in %s at line %d\n", cudaGetErrorString( err_a),
        __FILE__, __LINE__);
        exit(EXIT_FAILURE);
        }
    cudaMalloc((void **) &d_B, size);
    cudaMemcpy(d_B, B, size, cudaMemcpyHostToDevice);
    cudaMalloc((void **) &d_C, size);
    dim3 threadsPerBlock(n, m);
    vecAddKernelm<<<1,threadsPerBlock>>>(d_A, d_B, d_C,n,m);
    gpuErrchk( cudaPeekAtLastError() );
    cudaError_t err_c = cudaMemcpy(C, d_C, size, cudaMemcpyDeviceToHost);
    if (err_c != cudaSuccess) {
        printf("%s in %s at line %d\n", cudaGetErrorString( err_c),
        __FILE__, __LINE__);
        exit(EXIT_FAILURE);
        }
    for (int j=0; j< ROWS; j++)
        for (int i =0; i< COLS; i++)
            printf("%f\n",C[i][j]);
    cudaFree(d_A); cudaFree(d_B); cudaFree (d_C);
} 

int main() {
float A[ROWS][COLS], B[ROWS][COLS], C[ROWS][COLS];
for (int j=0; j< ROWS; j++)
    for (int i =0; i< COLS; i++)
        A[i][j] = j*i;
for (int j=0; j< ROWS; j++)
    for (int i =0; i< COLS; i++)
                B[i][j] = j+i;

VecAdd<COLS>(A,B,C,ROWS);
return 0;
}