```
nvcc adder.cu -g -G -arch=sm_61 -o adder -rdc=true
cuda-gdb adder
```
For some reason multiply.cu is not producing correct results. See Q2 for more details. 
https://devblogs.nvidia.com/cuda-pro-tip-write-flexible-kernels-grid-stride-loops/ 


Q3. Should work, refer to this : https://stackoverflow.com/questions/9936784/how-to-call-a-host-function-in-a-cuda-kernel . However, I would preferrably use global because he is working for a bigger organisation here. Also, remeber device functions get inlined when called by global 


Q5.(C) https://www3.nd.edu/~zxu2/acms60212-40212-S12/Lec-12-02.pdf 


Q6. (D) [Correct answer is (C)]


Q7. (C)

https://cs.nyu.edu/courses/spring17/CSCI-UA.0480-003/answers.pdf

